#[derive(Debug, PartialEq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

fn is_sublist<T: PartialEq>(short_list: &[T], long_list: &[T]) -> Comparison {
    let mut comparison = Comparison::Unequal; // Assume unequal
    let mut short_list_index = 0;

    if short_list.len() == 0 {
        return Comparison::Sublist; // Nothing is a sublist of anything
    }

    for (i, element) in long_list.iter().enumerate() {
        // Make sure that short_list_index doesn't go past bounds and that all of short_list will be checked
        if short_list_index < short_list.len() && long_list.len() - i >= short_list.len()  {
            if &short_list[short_list_index] == element {
                comparison = Comparison::Sublist;
                short_list_index += 1;
            } else {
                comparison = Comparison::Unequal;
                short_list_index = 0;
            }
        } else {
            break;
        }
    }

    return comparison;
}


pub fn sublist<T: PartialEq>(_first_list: &[T], _second_list: &[T]) -> Comparison {
    if _first_list.eq(_second_list) { // Check if they're equal first
        return Comparison::Equal;
    }

    let first_len_lte = _first_list.len() <= _second_list.len(); // Determine which list is larger

    if !first_len_lte { 
        let raw_result = is_sublist(_second_list, _first_list);

        match raw_result {
            // Swap Sublist and Superlist if comparing _second_list to _first_list
            Comparison::Sublist => return Comparison::Superlist,
            _ => return raw_result,
        }
    } else {
        return is_sublist(_first_list, _second_list);
    }
}
