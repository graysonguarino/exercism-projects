use enum_iterator::IntoEnumIterator;
use int_enum::{IntEnum, IntEnumError};

#[repr(usize)]
#[derive(Debug, Copy, Clone, Eq, PartialEq, IntEnum, IntoEnumIterator)]
pub enum ResistorColor {
    Black = 0,
    Brown = 1,
    Red = 2,
    Orange = 3,
    Yellow = 4,
    Green = 5,
    Blue = 6,
    Violet = 7,
    Grey = 8,
    White = 9,
}

impl ResistorColor {
    pub fn to_string(&self) -> String {
        format!("{:?}", &self)
    }
}

pub fn color_to_value(_color: ResistorColor) -> usize {
    return _color.int_value();
}

pub fn value_to_color_string(value: usize) -> String {
    let color: Result<ResistorColor, IntEnumError<ResistorColor>> = ResistorColor::from_int(value);
    
    match color {
        Ok(treated_color) => return treated_color.to_string(),
        Err(_) => return "value out of range".to_string(),
    }
}

pub fn colors() -> Vec<ResistorColor> {
    let mut color_vec: Vec<ResistorColor> = vec![];

    for color in ResistorColor::into_enum_iter() {
        color_vec.push(color);
    }

    return color_vec;
}
