// This stub file contains items which aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

#[derive(Clone)]
pub struct Player {
    pub health: u32,
    pub mana: Option<u32>,
    pub level: u32,
}

impl Player {
    pub fn revive(&self) -> Option<Player> {
        let mut new_player = self.clone();

        match (&self.health, &self.level) {
            (0, 10..) => {
                new_player.health = 100; 
                new_player.mana = Some(100);
            },
            (0, _) => {
                new_player.health = 100;
                new_player.mana = None;
            },
            (_, _) => {
                return None;
            }
        }
        return Some(new_player);
    }

    pub fn cast_spell(&mut self, mana_cost: u32) -> u32 {
        match &self.mana {
            Some(player_mana) => {
                if player_mana >= &mana_cost {
                    self.mana = Some(&self.mana.unwrap() - &mana_cost);
                    return 2 * mana_cost;
                } else {
                    return 0;
                }
            },
            None => {
                self.health = self.health.checked_sub(mana_cost).unwrap_or(0); // Prevent overflow
                return 0;
            }
        }
    }
}
