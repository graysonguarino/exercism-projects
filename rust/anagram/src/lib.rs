use std::collections::HashSet;

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    let lowercase_word = word.to_lowercase();

    let mut word_chars: Vec<char> = lowercase_word.chars().collect();
    word_chars.sort_unstable();

    let mut anagram_hashset: HashSet<&'a str> = HashSet::new();
    for &v in possible_anagrams {
        let lowercase_anagram = v.to_lowercase();
        let mut anagram_chars: Vec<char> = lowercase_anagram.chars().collect();
        anagram_chars.sort_unstable();

        if anagram_chars == word_chars && lowercase_word != lowercase_anagram {
            anagram_hashset.insert(v);
        }
    }

    return anagram_hashset;
}
