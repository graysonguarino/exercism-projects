#[derive(Debug)]
pub enum CalculatorInput {
    Add,
    Subtract,
    Multiply,
    Divide,
    Value(i32),
}

pub fn evaluate(inputs: &[CalculatorInput]) -> Option<i32> {
    
    fn operate(stack: &mut Vec<Option<i32>>, op: &CalculatorInput) {
        let val_2 = stack.pop().unwrap_or(None);
        let val_1 = stack.pop().unwrap_or(None);

        if val_1 == None || val_2 == None {
            stack.push(None);
        } else {
            match op {
                &CalculatorInput::Add => stack.push(Some(val_1.unwrap() + val_2.unwrap())),
                &CalculatorInput::Subtract => stack.push(Some(val_1.unwrap() - val_2.unwrap())),
                &CalculatorInput::Multiply => stack.push(Some(val_1.unwrap() * val_2.unwrap())),
                &CalculatorInput::Divide => stack.push(Some(val_1.unwrap() / val_2.unwrap())),
                _ => stack.push(None),
            }
        }
    }

    let mut stack: Vec<Option<i32>> = vec![];

    for op in inputs {
        match op {
            &CalculatorInput::Value(value) => stack.push(Some(value)),
            _ => operate(&mut stack, op),
        }
    }

    if stack.len() == 1 {
        return stack.pop()?;
    }
    return None;
}
