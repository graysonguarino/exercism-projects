// This stub file contains items which aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

use std::collections::HashMap;

pub fn can_construct_note(magazine: &[&str], note: &[&str]) -> bool {
    let mut map = HashMap::new();
    
    for word in magazine {
        let counter = map.entry(word).or_insert(0);
        *counter += 1;
    }

    for word in note {
        let counter = map.entry(word).or_insert(0);

        if *counter == 0 {
            return false;
        } else {
            *counter -= 1;
        }
    }

    return true;
}