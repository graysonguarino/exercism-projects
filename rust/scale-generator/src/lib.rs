// You should change this.
//
// Depending on your implementation, there are a variety of potential errors
// which might occur. They aren't checked by the test suite in order to
// allow the greatest freedom of implementation, but real libraries should
// provide useful, descriptive errors so that downstream code can react
// appropriately.
//
// One common idiom is to define an Error enum which wraps all potential
// errors. Another common idiom is to use a helper type such as failure::Error
// which does more or less the same thing but automatically.
#[derive(Debug)]
pub struct Error;

const SHARP_NOTES: [&str; 12] = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"];
//    MAJ                       [ #    b    b/#   !    b/#   #    b     #    b    b/#   #     b ]
//    MIN                       [ !   b/#    #    b     #    b   #/b    #    b     #    b    b/#]
const FLAT_NOTES: [&str; 12] = ["A", "Bb", "B", "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab"];

struct Key {
    root: usize,
    is_sharp: bool,
}

pub struct Scale {
    scale: Vec<String>, // The elements of this array must be in format "Ab", "C#", etc
}

impl Scale {

    fn tonic_to_key(tonic: &str) -> Result<Key, Error> {
        let is_major = tonic.chars().nth(0).unwrap().is_uppercase();

        let is_sharp = 
            match tonic.to_lowercase().chars().collect::<Vec<char>>()[..] {
                ['a'] => true,
                ['b'] => true,
                ['c'] => is_major,
                ['d'] => is_major,
                ['e'] => true,
                ['f'] => false,
                ['g'] => is_major,
                [_, 'b'] => false,
                [_, '#'] => true,
                _ => false,
            };

        match tonic.to_lowercase().as_str() {
            "a" => Ok(Key{root: 0, is_sharp}),
            "a#" | "bb" => Ok(Key{root: 1, is_sharp}),
            "b" => Ok(Key{root: 2, is_sharp}),
            "c" => Ok(Key{root: 3, is_sharp}),
            "c#" | "db" => Ok(Key{root: 4, is_sharp}),
            "d" => Ok(Key{root: 5, is_sharp}),
            "d#" | "eb" => Ok(Key{root: 6, is_sharp}),
            "e" => Ok(Key{root: 7, is_sharp}),
            "f" => Ok(Key{root: 8, is_sharp}),
            "f#" | "gb" => Ok(Key{root: 9, is_sharp}),
            "g" => Ok(Key{root: 10, is_sharp}),
            "g#" | "ab" => Ok(Key{root: 11, is_sharp}),
            _ => Err(Error),
        }
    }

    fn intervals_to_distances(intervals: &str) -> Result<Vec<usize>, Error> {
        let mut distances = vec![];
        
        for (i, element) in intervals.chars().enumerate() {
            match element {
                'm' => distances.insert(i, 1),
                'M' => distances.insert(i, 2),
                'A' => distances.insert(i, 3),
                _ => return Err(Error),
            }
        }

        return Ok(distances);
    }

    fn format_notes(key: Key, distances: Vec<usize>) -> Scale {
        let mut notes= Scale { scale: vec![] };
        let mut current_note_index = key.root;

        if key.is_sharp { // TODO: Clean duplicated code
            notes.scale.push(SHARP_NOTES[current_note_index].to_string());
        } else {
            notes.scale.push(FLAT_NOTES[current_note_index].to_string());
        }

        for distance in distances {
            current_note_index = (current_note_index + distance) % 12;
            if key.is_sharp {
                notes.scale.push(SHARP_NOTES[current_note_index].to_string());
            } else {
                notes.scale.push(FLAT_NOTES[current_note_index].to_string())
            }
        }

        return notes;
    }

    pub fn new(tonic: &str, intervals: &str) -> Result<Scale, Error> {
        let key = Scale::tonic_to_key(tonic).unwrap(); // No error handling at the moment
        let distances = Scale::intervals_to_distances(intervals).unwrap();
        
        return Ok(Scale::format_notes(key, distances)); // TODO : Error?
    }

    pub fn chromatic(tonic: &str) -> Result<Scale, Error> {
        let key = Scale::tonic_to_key(tonic).unwrap();
        let distances = [1; 12].to_vec();

        return Ok(Scale::format_notes(key, distances));
    }

    pub fn enumerate(&self) -> Vec<String> {
        return self.scale.clone();
    }
}