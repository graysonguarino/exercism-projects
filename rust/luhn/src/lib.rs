/// Perform Luhn on a vector of unsigned ints
fn luhn(mut digits: Vec<u32>) -> bool {
    // Double every second digit, starting from right
    let len = digits.len();
    let is_len_even = len % 2 == 0;

    for i in (0..len).rev() {
        if (is_len_even && i % 2 == 0) || (!is_len_even && i % 2 != 0) {
            let doubled_digit = digits[i] * 2;

            // If doubling the number results in a number greater than 9 then subtract 9 from the product
            match doubled_digit > 9 {
                true => digits[i] = doubled_digit - 9,
                false => digits[i] = doubled_digit,
            };
        }
    };

    // Sum all the digits
    let mut digit_sum: u32 = 0;
    for digit in digits {
        digit_sum += digit;
    };

    // If sum is divisible by 10, return true
    matches!(digit_sum % 10, 0)
}

/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    // Convert code to char vec, filtering out whitespaces
    let filtered_code_chars: Vec<char> = code.chars().filter(|x| x != &' ').collect();
    
    // Strings of length 1 or less are not valid
    if filtered_code_chars.len() <= 1 {
        return false;
    }

    let mut digits = vec![];
    for digit in filtered_code_chars {
        match digit.to_digit(10) {
            Some(n) => digits.push(n),
            None => return false,
        };
    }

    luhn(digits)
}
