const SECONDS_IN_A_YEAR: f64 = 31_557_600.0;

pub struct Mercury;
pub struct Venus;
pub struct Earth;
pub struct Mars;
pub struct Jupiter;
pub struct Saturn;
pub struct Uranus;
pub struct Neptune;

#[derive(Debug)]
pub struct Duration {
    s: u64,
}

impl From<u64> for Duration {
    fn from(s: u64) -> Self {
        return Duration{s};
    }
}

pub trait Planet {
    fn years_during(d: &Duration) -> f64;
    fn get_period(&self) -> f64;
}

macro_rules! impl_Planet {
    (for $(($planet:ty, $period:tt)),+) => {
        $(impl Planet for $planet {
            fn years_during(d: &Duration) -> f64 {
                return (d.s as f64 / SECONDS_IN_A_YEAR) / Self::get_period(&Self);
            }
            fn get_period(&self) -> f64 {
                return $period;
            }
        })*
    }
}

impl_Planet!(for (Mercury, 0.2408467), (Venus, 0.61519726), (Earth, 1.0), (Mars, 1.8808158), (Jupiter, 11.862615),
            (Saturn, 29.447498) , (Uranus, 84.016846), (Neptune, 164.79132));