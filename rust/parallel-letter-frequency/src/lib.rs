// This solution uses crossbeam to allow for scoped threads
use std::collections::HashMap;

fn count(input: &[&str]) -> HashMap<char, usize> {
    let mut letter_map: HashMap<char, usize> = HashMap::new();

    for line in input {
        let letters = line.to_lowercase();
        let letters = letters.chars().filter(|c| c.is_alphabetic());

        // If letter in letter_map, increment count by one. Else, insert 0 value then increment by one.
        for letter in letters {
            let count = letter_map.entry(letter).or_insert(0);
            *count += 1;
        }
    }

    letter_map
}

pub fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {
    let mut letter_map: HashMap<char, usize> = HashMap::new();

    let input_len = input.len();
    // Return early if input is empty
    if input_len == 0 {
       return letter_map;
    }

    // Divide the list into chunks for worker_count (divide rounding up)
    let chunk_size = (input_len + worker_count - 1) / worker_count;
    let chunks = input.chunks(chunk_size);

    // Create threads for each worker
    crossbeam::thread::scope(|s| {
        let mut handles = vec![];
        
        for chunk in chunks {
            let handle = s.spawn(|_| {
                count(chunk)
            });

            handles.push(handle);
        }

        // Add results from workers' handles into return letter_map
        for handle in handles {
            let map = handle.join().unwrap();
            // Merge HashMaps, incrementing value.
            for (k, v) in map {
                let count = letter_map.entry(k).or_insert(0);
                *count += v;
            }
        }

        letter_map
    }).unwrap()
}
