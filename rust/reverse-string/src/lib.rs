use unicode_segmentation::UnicodeSegmentation;

pub fn reverse(input: &str) -> String {
    let mut graphemes = input.graphemes(true).collect::<Vec<&str>>();
    graphemes.reverse();
    return String::from(graphemes.concat());
}