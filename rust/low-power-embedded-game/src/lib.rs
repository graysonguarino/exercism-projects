// This stub file contains items which aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

use std::any::{Any, TypeId};


pub fn divmod(dividend: i16, divisor: i16) -> (i16, i16) {
    let div = dividend / divisor;
    let modulo = dividend % divisor;
    return (div, modulo);
}

pub fn evens<T>(iter: impl Iterator<Item = T>) -> impl Iterator<Item = T>
{
    return iter.step_by(2);
}

pub struct Position(pub i16, pub i16);
impl Position {
    pub fn manhattan(&self) -> i16 {
        let Position(x, y) = self;
        return x.abs() + y.abs();
    }
}
